"""
project3.py

UVA CS 1120 -- Problem Set 3 -- Limning L-System Fractals

Your name: [ Kyle Matthews & Caitlyn Mulcahy ]
Your ID:   [ KEM5HK - CEM3CR]
"""

from lsystem import *
from curves import *
from graphics import *

# Exercise 1: Remember to finish the functions in lsystem.py

# Problem 1

def flatten_commands(commands):
    if len(commands) == 0:
        return []
    else:
        if is_lsystem_command(commands[0]):
            return commands[0] + flatten_commands(commands[1:])
        else:
            return flatten_commands(commands[0]) + flatten_commands(commands[1:])




    """
    Returns a flat list containing all of the L-System commands in the input in
    the same order.
    """
#    pass # Replace with your code

# Problem 2

def rewrite_lcommands(lcommands, replacement):
    if len(lcommands) == 0:
        return []
    else:
        if is_forward(lcommands[0]):
                return replacement + rewrite_lcommands(lcommands[1:],replacement)
        else:
             return lcommands[0] + rewrite_lcommands(lcommands[1:],replacement)




    """
    Returns a list of L-System commands that results from replacing all of the Forward commands
    in the lcommands input (including those inside offshoots) with the sequence of commands
    given by replacement.
    """
    pass # Replace with your code

# Problem 3

def vertical_mid_line(t):
    return make_point(0.5,t)

# Problem 4

def make_vertical_line(position):
    def new_point(t):
        return make_point(position,t)
    return new_point



# Problem 5

def half_line(t):
    return make_point(0.5,0.5)

half_line = translate(horiz_line, 0.5, 0.5) 

draw_curve_points(half_line, 1000)

# draw_curve_points(mid_line, 1000)
# draw_curve_points(shrink(mid_line, 0.5), 1000)
# draw_curve_points(half_line, 1000)

# Problem 6

def num_points(npoints, curve_num):
    """
    Returns the number of points that will be drawn in curve curve_num for
    when drawing
       connect_rigidly(c1, connect_rigidly(c2, connect_rigidly(c3, ... c_curve_num)))

    The first argument to num_points is the number of points (t-values) total to draw.
    The second argument is the number of curves (including the one we want to know
    the number of points for).
    """
    # We have provided a base case:
    if curve_num == 1:      # if there is one curve left
        return npoints      # it gets all of the remaining points
    else:                   # otherwise, the rest of the curves
        pass # finish       # get half of the remaining points

# Problems 7 and 8 - add your code below

def convert_lcommands_to_curvelist(lcommands):
    """
    Returns a curve that corresponds to the list of L-System commands.
    """
    if len(lcommands) == 0:
        # We need to make a leaf with just a single point of green
        def draw_green_point(t):
            return make_colored_point(0, 0, GREEN)
        return draw_green_point
    elif is_forward(lcommands[0]):
        # Make a vertical_line and recurse on the rest of list
        return construct_simple_curvelist(vertical_line,
                                          convert_lcommands_to_curvelist(lcommands[1:]))
    elif is_rotate(lcommands[0]):
        # If this command is a rotate, every curve in the rest
        # of the list should be rotated by the rotate angle
        # Lsystem turns are clockwise, so we need to use - angle.
        rotate_angle = -1 * get_angle(lcommands[0])
        # Problem 7: complete code for rotating
        raise RuntimeError("Rotate not yet implemented (Problem 7)")
    elif is_offshoot(lcommands[0]):
        # Problem 8: complete code for offshoots
        raise RuntimeError("Rotate not yet implemented (Problem 8)")
    else:
        raise RuntimeError("Bad Command")

# Problem 9

def make_lsystem_fractal(replace_commands, start, level):
    """
    Returns a sequence of L-System commands that results from repeating the replacement rule
    level times, starting from start and replacing F's with replace_commands.
    """
    raise RuntimeError("Not yet implemented: Problem 9")

def make_tree_fractal(level):
    """

    """
    return make_lsystem_fractal(TREE_COMMANDS, [make_forward_command()], level)

def draw_lsystem_fractal(lcommands):
    """
    Draws the L-System fractal described by lcommands.
    """
    draw_curve_points(
        position_curve(
            connect_curves_evenly(
                convert_lcommands_to_curvelist(lcommands)), 
            0.5, 0.1), 
        5000)

# Problem 10

# Put the code for drawing your most amazing image here.  You should feel
# free to define any procedures you want for this, and make any changes
# you want to provided code (just make it clear what you did in your answer).


# Uncomment this when you are ready to draw (and replace with code that
# draws your own fractal design).

# draw_lsystem_fractal(make_tree_fractal(3))

# This saves your last image as "fractal.svg".
save_image("fractal.svg")       # To make our pretty graphics work,
turtle.exitonclick()            # THESE MUST BE THE LAST LINES HERE!
